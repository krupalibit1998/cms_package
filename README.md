If you're using Laravel 8, this is all there is to do. 

Now all you have to do is add the service provider of the package and alias the package. To do this open your `config/app.php` file.
Should you still be on version 8 of Laravel, the final steps for you are to add the service provider of the package and alias the package. To do this open your `config/app.php` file.

Add a new line to the `providers` array:

	Cms\\Cmspackage\\CmsServiceProvider::class
And optionally add a new line to the `aliases` array:
Now you're ready to start using the shoppingcart in your application.
**As of version 1 of this package it's possibly to use dependency injection to inject an instance of the Cart class into your controller or other class**
## Overview
Look at one of the following topics to learn more about LaravelCms
* [Usage](#usage)
* [Collections](#collections)
* [Instances](#instances)
* [Models](#models)
* [Database](#database)
* [Exceptions](#exceptions)
* [Events](#events)
* [Example](#example)
## Usage
The shoppingcart gives you the following methods to use:
**N.B. Keep in mind that the cms stays in the last set instance for as long as you don't set a different one during script execution.**
### Configuration
To save cart into the database so you can retrieve it later, the package needs to know which database connection to use and what the name of the table is.
By default the package will use the default database connection and use a table named `shoppingcart`.
If you want to change these options, you'll have to publish the `config` file.
    php artisan vendor:publish --provider="Cms\Cmspackage\CmsServiceProvider" --tag="config"
This will give you a `cms.php` config file in which you can make the changes.
To make your life easy, the package also includes a ready to use `migration` which you can publish by running:
    php artisan vendor:publish --provider="Cms\Cmspackage\CmsServiceProvider" --tag="migrations"
    
This will place a `Cmsdatabase` table's migration file into `database/migrations` directory. Now all you have to do is run `php artisan migrate` to migrate your database.
